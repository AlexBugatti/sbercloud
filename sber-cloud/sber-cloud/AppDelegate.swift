//
//  AppDelegate.swift
//  sber-cloud
//
//  Created by Александр on 27.02.2021.
//

import UIKit
import SberbankSDK

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let mainRouter = MainRouter()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: R.font.sbSansInterfaceSemibold(size: 20)]
        UITabBar.appearance().tintColor = Pallete.darkGrey
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: R.font.sbSansInterfaceRegular(size: 10)!], for: .normal)
        
        
        mainRouter.didOpenDashboard = {
            self.window?.rootViewController = self.mainRouter.rootController
        }
        self.window?.rootViewController = mainRouter.rootController
        
        return true
    }
    
    func application(_ app: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any] ) -> Bool {
        if url.scheme == "sbercloud" && url.host == "sberidauth" {
            SBKAuthManager.getResponseFrom(url) { response in
                //do something
            }
        }
        return true
    }

//    func applicationDidBecomeActive(_ application: UIApplication) {
//        if self.mainRouter.auth == true {
//            self.mainRouter.gotodashboard()
//        }
//    }
    
}

