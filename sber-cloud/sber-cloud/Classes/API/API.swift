//
//  API.swift
//  Beauty Profi
//
//  Created by Petr on 04.07.2020.
//  Copyright © 2020 Petr. All rights reserved.
//

import Foundation
import Alamofire

class API {
    
    enum Endpoint: String {
        case debug = "http://35.225.48.212/sbercloud/services/"
        case release = "http://35.225.48.212/sbercloud/services"
    }
    
    static var url: Endpoint {
        #if DEBUG
        print(Endpoint.debug)
        return Endpoint.debug
        #else
        print(Endpoint.release)
        return Endpoint.release
        #endif
    }
    
    static func showErrorAlert(text: String) {
        
        let topVC = UIViewController.topViewController
        
        let accessErrorString = "API не найдено или отсутствует доступ!"
        //        if text.contains(accessErrorString) {
        topVC?.showWarningAlert(text: accessErrorString, title: "Ошибка", type: .error)
        //        }
        
        //        topVC?.showSystemAlert(text: text)
    }
    
    static func upload(path: inout String, dataParameters: [String: String]? = nil, pathParameters: [String: String]?, method: HTTPMethod = .post, needToken: Bool = false, requestEnd: @escaping ([String: Any]) -> ()) {
        var headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Accept": "application/json"
        ]
        
        if needToken {
            guard addToken(headers: &headers) else {
                requestEnd([:])
                return
            }
        }
        
        if let keys = pathParameters?.keys {
            for key in keys.enumerated() {
                guard let value = pathParameters?[key.element] else { return }
                if key.offset == 0 {
                    path += "?\(key.element)=\(value)"
                } else {
                    path += "&\(key.element)=\(value)"
                }
                
            }
        }
        
        var createdDataParameters = [String: Data]()
        if let dataParameters = dataParameters {
            createdDataParameters = createParameterDictionary(from: dataParameters)
        }
        
        var _response: [String: Any] = [:]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            for (key, value) in createdDataParameters {
                multipartFormData.append(value, withName: key)
            }
        }, to: url.rawValue + path, method: .post, headers: headers) { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    //                    debugPrint(response)
                    _response = getResponse(from: response)
                    requestEnd(_response)
                }
            case .failure(let encodingError):
                showErrorAlert(text: encodingError.localizedDescription)
                requestEnd(_response)
            //                print(encodingError)
            }
        }
    }
    
    static func upload(path: inout String, dataParameters: [[String: Any]], pathParameters: [String: String]?, method: HTTPMethod = .post, needToken: Bool = false, requestEnd: @escaping ([String: Any]) -> ()) {
        var headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Accept": "application/json"
        ]
        
        if needToken {
            guard addToken(headers: &headers) else {
                requestEnd([:])
                return
            }
        }
        
        if let keys = pathParameters?.keys {
            for key in keys.enumerated() {
                guard let value = pathParameters?[key.element] else { return }
                if key.offset == 0 {
                    path += "?\(key.element)=\(value)"
                } else {
                    path += "&\(key.element)=\(value)"
                }
                
            }
        }
        
        
        guard let data = try? JSONSerialization.data(withJSONObject: dataParameters, options: []) else {
            return 
        }
        let dataString = String(data: data, encoding: String.Encoding.utf8)
        
        var _response: [String: Any] = [:]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            //                for (key, value) in dataParameters {
            multipartFormData.append(data, withName: "filters")
            //                }
        }, to: url.rawValue + path, method: .post, headers: headers) { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    //                    debugPrint(response)
                    _response = getResponse(from: response)
                    requestEnd(_response)
                }
            case .failure(let encodingError):
                showErrorAlert(text: encodingError.localizedDescription)
                requestEnd(_response)
            //                print(encodingError)
            }
        }
    }
    
    
    static func request(path: inout String, parameters: [String: Any]? = nil, method: HTTPMethod = .get, needToken: Bool = true, requestEnd: @escaping ([String: Any]) -> ()) {
        
//        var headers: [HTTPHeaders] = nil // = ["Accept": "application/json",
//                       "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"]
        
//        if needToken {
//            guard addToken(headers: &headers) else {
//                requestEnd([:])
//                return
//            }
//        }
        path += "?"
        if let keys = parameters?.keys {
            for key in keys.enumerated() {
                guard let value = parameters?[key.element] else { return }
                if key.offset == 0 {
                    path += "\(key.element)=\(value)"
                } else {
                    path += "&\(key.element)=\(value)"
                }
                
            }
        }
        
        
        let updatedParameters = method == .get ? nil : parameters
        
        var _response: [String: Any] = [:]
        
        Alamofire.request(url.rawValue + path, method: method, parameters: updatedParameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            
            _response = getResponse(from: response)
            requestEnd(_response)
        }
    }
    
    private static func createParameterDictionary(from parameters: [String: String]) -> [String: Data] {
        var dictionary: [String: Data] = [:]
        for key in parameters.keys {
            dictionary[key] = parameters[key]?.data(using: .utf8)
        }
        return dictionary
    }
    
    private static func getResponse(from response: DataResponse<Any>) -> [String: Any] {
        
        let code = response.response?.statusCode
        
        guard code != 401 else {
            print("[API] Session is invalid")
            
//            if Global.isAuthorized {
//                Global.logout()
//                UIViewController.topViewController?.showWarningAlert(text: "Время сессии истекло. Необходима повторная авторизация", type: .warning)
//            }
            
            return ["errors": "Session is invalid"]
        }
        
        if code != 200 {
            print("[API] Bad status code: \(code ?? -1)")
        }
        
        guard let content = response.result.value as? [String: Any] else {
            print("[API] Can't get content from response")
            return [:]
        }
        
        if let _ = content["error_code"] as? Int {
            UIViewController.topViewController?.showWarningAlert(text: content["error_text"] as? String ?? "", type: .error )
        }
        
        var result = [String: Any]()
        result = content
//        if let response = content as? [String: Any] {
//            result = response
//        } else {
//            print("[API] Can't get response in valid result")
//            return [:]
//        }
        
        return result
    }
    
    private static func addToken(headers: inout [String: String]) -> Bool {
        
        //        guard Global.isAuthorized else {
        //            print("[API] Token is empty when reqiered")
        //            return false
        //        }
        
//        headers["X-Beauty-Fly-Token"] = Global.token
        
        return true
    }
    
    static func upload(path: inout String, dataParameters: [String: Any], requestEnd: @escaping ([String: Any]) -> ()) {
        var headers: HTTPHeaders = [
            "Content-type": "multipart/form-data",
            "Accept": "application/json"
        ]
        
        guard addToken(headers: &headers) else {
            requestEnd([:])
            return
        }
        
        var _response: [String: Any] = [:]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
//            dataParameters.keys.forEach({
//                multipartFormData.append(Data("\(dataParameters[$0] ?? "")".utf8), withName: $0)
//            })
            
            for item in dataParameters.keys {
                debugPrint("\(item)=  \(dataParameters[item])")
                if let data = "\(dataParameters[item] ?? "")".data(using: .utf8, allowLossyConversion: false) {
                    multipartFormData.append(data, withName: item)
                }
            }
            
        }, to: url.rawValue + path, method: .post, headers: headers) { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    //                    debugPrint(response)
                    _response = getResponse(from: response)
                    requestEnd(_response)
                }
            case .failure(let encodingError):
                showErrorAlert(text: encodingError.localizedDescription)
                requestEnd(_response)
            //                print(encodingError)
            }
        }
    }
}
