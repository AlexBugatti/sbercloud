//
//  SystemModule.swift
//  sber-cloud
//
//  Created by Александр on 27.02.2021.
//

import Foundation

extension API {
    
    class CloudEyeModule {
        static let basicpath = "cloud.eye/"

        enum Metric: String {
            case cpu
            case disk_read_bytes_rate
            case disk_read_requests_rate
            case disk_write_bytes_rate
            case disk_write_requests_rate
            case network_incoming_bytes_aggregate_rate
            case network_outgoing_bytes_aggregate_rate
        }
        
        static func get(metric: Metric, from: Date, to: Date, id: String? = nil, completion: @escaping ((MetricResponse?)->Void)) {
            
            var path = "\(CloudEyeModule.basicpath)\(metric)"
            var params: [String: Any] = [  "to": from.timeIntervalSince1970.toInt(),
                                           "from": to.timeIntervalSince1970.toInt()]
            if let id = id {
                params["id"] = id
            }
            
            API.request(path: &path, parameters: params, method: .get, needToken: false) { (content) in
                print(content)
                
                do {
                    let modelData = try JSONSerialization.data(withJSONObject: content, options: [])
                    let metric = try JSONDecoder().decode(MetricResponse.self, from: modelData)
                    completion(metric)
                } catch {
                    completion(nil)
                }

            }
        }
        
        static func getCatalog(completion: @escaping (([ServiceModel])->Void)) {
            
            var path = "\(CloudEyeModule.basicpath)/metrics/"
            
            API.request(path: &path, parameters: nil, method: .get, needToken: false) { (content) in
                print(content)
                
                do {
                    let metrics = content["metrics"] as? [[String: Any]] ?? []
                    let modelData = try JSONSerialization.data(withJSONObject: metrics, options: [])
                    let metric = try JSONDecoder().decode([ServiceModel?].self, from: modelData)
                    completion(metric.compactMap({ $0 }))
                } catch {
                    print(error.localizedDescription)
                    completion([])
                }

            }
        }
        
    }
    
}

extension TimeInterval {
    
    func toString() -> String {
        let value = Int(self * 1000)
        return "\(value)"
    }
    
    func toInt() -> Int {
        return Int(self * 1000)
    }
    
}
