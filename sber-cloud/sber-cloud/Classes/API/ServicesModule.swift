//
//  ServicesModule.swift
//  sber-cloud
//
//  Created by Александр on 28.02.2021.
//

import Foundation

extension API {
    
    class ServicesModule {
        static let basicpath = "cloud.eye/"

        enum Metric: String {
            case cpu
            case disk_read_bytes_rate
            case disk_read_requests_rate
            case disk_write_bytes_rate
            case disk_write_requests_rate
            case network_incoming_bytes_aggregate_rate
            case network_outgoing_bytes_aggregate_rate
        }
        
        static func get(metric: Metric, from: Date, to: Date, id: String? = nil, completion: @escaping ((MetricResponse?)->Void)) {
            
            var path = "\(CloudEyeModule.basicpath)\(metric)"
            var params: [String: Any] = [  "to": from.timeIntervalSince1970.toInt(),
                                           "from": to.timeIntervalSince1970.toInt()]
            if let id = id {
                params["id"] = id
            }
            
            API.request(path: &path, parameters: params, method: .get, needToken: false) { (content) in
                print(content)
                
                do {
                    let modelData = try JSONSerialization.data(withJSONObject: content, options: [])
                    let metric = try JSONDecoder().decode(MetricResponse.self, from: modelData)
                    completion(metric)
                } catch {
                    completion(nil)
                }

            }
        }
        
    }
    
}
