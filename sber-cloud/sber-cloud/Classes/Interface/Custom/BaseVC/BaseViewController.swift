//
//  BaseViewController.swift
//  sber-cloud
//
//  Created by Александр on 28.02.2021.
//

import UIKit
import NVActivityIndicatorView

class BaseViewController: UIViewController {

    lazy var activity: NVActivityIndicatorView = {
        var view = NVActivityIndicatorView(frame: CGRect.init(x: 50, y: 50, width: 50, height: 50), type: .ballScaleMultiple, color: Pallete.green)
        view.translatesAutoresizingMaskIntoConstraints = false
       return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(activity)
        activity.layout.centerSuperview()
        activity.layout.widthEqualTo(50)
        activity.layout.heightEqualTo(50)
        // Do any additional setup after loading the view.
    }
    
    func showActivity() {
        activity.startAnimating()
    }
    
    func hideActivity() {
        activity.stopAnimating()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
