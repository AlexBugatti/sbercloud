//
//  PlotView.swift
//  sber-cloud
//
//  Created by Александр on 28.02.2021.
//

import UIKit
import Charts

class PlotView: UIView {

    var metric: MetricResponse!
    private lazy var stackView: UIStackView = {
        var view = UIStackView()
        view.axis = .vertical
        view.distribution = .equalSpacing
        view.spacing = 8
        
        return view
    }()
    private lazy var panelStackView: UIStackView = {
        var view = UIStackView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .horizontal
        view.distribution = .fillProportionally
        view.spacing = 8
        view.layout.heightEqualTo(24)
        
        return view
    }()
    private lazy var titleLabel: UILabel = {
        var label = UILabel()
        label.font = R.font.sbSansInterfaceRegular(size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
//        label.layout.heightEqualTo(30)
        return label
    }()
    private lazy var calendarButton: UIButton = {
        var button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layout.widthEqualTo(24)
        button.setImage(#imageLiteral(resourceName: "calendar"), for: .normal)
        return button
    }()
    private lazy var barView: LineChartView = {
        var view = LineChartView.init()
        view.translatesAutoresizingMaskIntoConstraints = false
//        view.xAxis.drawGridLinesEnabled = false
//        view.xAxis.drawLabelsEnabled = false
//        view.rightAxis.drawAxisLineEnabled = false
//        view.rightAxis.drawTopYLabelEntryEnabled = true
//        view.drawGridBackgroundEnabled = false
        view.legend.enabled = false
//        view.xAxis.spaceMin = 0
//        view.xAxis.spaceMax = 0
//        view.rightAxis.enabled = false
//        view.legend.enabled = false
        view.xAxis.labelPosition = .bottom
        view.xAxis.enabled = true
        view.leftAxis.enabled = true
        view.rightAxis.enabled = false
//        view.xAxis.drawGridLinesEnabled = false
        view.layout.heightEqualTo(200)
//        view.backgroundColor = .red
        return view
    }()
    
    init(metric: MetricResponse) {
        self.metric = metric
        super.init(frame: CGRect.zero)
        common()
        updateChartWithData(metric: metric)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        common()
    }
    
    func common() {
//        backgroundColor = Pallete.darkGrey
//        layer.cornerRadius = 10
//        layer.masksToBounds = true
        addSubview(stackView)
        
        let paddingView = UIView()
        paddingView.translatesAutoresizingMaskIntoConstraints = false
        paddingView.addSubview(titleLabel)
        stackView.addArrangedSubview(paddingView)
        
        let view = UIView()
        let lastView = UIView()
        lastView.translatesAutoresizingMaskIntoConstraints = false
        lastView.layout.widthEqualTo(12)
        lastView.backgroundColor = .red
//        panelStackView.addArrangedSubview(view)
//        panelStackView.addArrangedSubview(calendarButton)
//        panelStackView.addArrangedSubview(lastView)
//        stackView.addArrangedSubview(panelStackView)
        stackView.addArrangedSubview(barView)
        
        stackView.fillSuperview()
        titleLabel.layout.fillSuperview(inset: UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 20))
    }
    
    func updateChartWithData(metric: MetricResponse) {
        
        let datapoints = metric.datapoints
        let dataEntries = (0..<datapoints.count).map { (i) -> ChartDataEntry in
            let value = datapoints[i]
            return ChartDataEntry(x: Double(i), y: value.min)
        }
//        for i in 0..<datapoints.count {
//            let value = datapoints[i]
//            let dataEntry = ChartDataEntry(x: Double(i), y: Double(value.min))
//            dataEntries.append(dataEntry)
//        }
        let chartDataSet = LineChartDataSet(entries: dataEntries, label: metric.metric_name)
        chartDataSet.setColor(Pallete.green)
        chartDataSet.drawCirclesEnabled = false
        chartDataSet.drawValuesEnabled = false
        chartDataSet.lineWidth = 3
        let gradientColors = [Pallete.green.cgColor, UIColor.white.cgColor] as CFArray // Colors of the gradient
        let colorLocations:[CGFloat] = [1.0, 0.25] // Positioning of the gradient
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradient(colorsSpace: colorSpace, colors: gradientColors , locations: colorLocations)
        chartDataSet.drawFilledEnabled = true
        chartDataSet.fill = Fill.init(CGColor: Pallete.green.cgColor)
        
//        chartDataSet.drawCirclesEnabled = false
//        chartDataSet.drawValuesEnabled = false
        let chartData = LineChartData(dataSet: chartDataSet)
        let unit = metric.datapoints.first?.unit ?? ""
        let title = "\(metric.metric_name.replacingOccurrences(of: "_", with: " ").uppercased()), \(unit)"
        titleLabel.text = title
//        barView.backgroundColor = .red

        
        barView.data = chartData
        barView.xAxis.valueFormatter = IndexAxisValueFormatter(values: datapoints.map({ $0.timestamp.toDate() }))
        
        
//
//        let yaxis = barView.getAxis(YAxis.AxisDependency.left)
//           yaxis.drawLabelsEnabled = false
//           yaxis.enabled = false
//
//        let xaxis = barView.getAxis(YAxis.AxisDependency.right)
//           xaxis.drawLabelsEnabled = false
//           xaxis.enabled = false
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension Int64 {
    
    func toDate() -> String {
        let date = Date.init(timeIntervalSince1970: TimeInterval(self))
        let formatter = DateFormatter.init()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: date) ?? ""
    }
    
}
