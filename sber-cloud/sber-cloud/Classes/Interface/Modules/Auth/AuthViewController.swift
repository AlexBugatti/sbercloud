//
//  AuthViewController.swift
//  sber-cloud
//
//  Created by Александр on 27.02.2021.
//

import UIKit
import SberbankSDK

class AuthViewController: UIViewController {

    var didAuthSuccess: (()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        // Do any additional setup after loading the view.
    }

    private func setup() {
        let loginButton = SBKLoginButton.init(type: .green)
        loginButton.addTarget(self, action: #selector(loginButtonDidTap(_:)), for: .touchUpInside)
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(loginButton)
        loginButton.layout.centerSuperview()
    }
    
    @objc func loginButtonDidTap(_ sender: SBKLoginButton) {
        
        let alert = UIAlertController.init(title: "Авторизация", message: nil, preferredStyle: .alert)
        let simple = UIAlertAction.init(title: "Войти", style: .default) { (action) in
            self.didAuthSuccess?()
        }
        let sber = UIAlertAction.init(title: "Sber ID", style: .default) { (action) in
            self.createSberRequest()
        }
        alert.addAction(simple)
        alert.addAction(sber)
        self.present(alert, animated: true, completion: nil)
//        self.didAuthSuccess?()
//        createSberRequest()
    }
    
    func createSberRequest() {
        let verifier = SBKUtils.createVerifier()
         
        let request = SBKAuthRequest()
        request.clientId = "0176eb5f-0683-460f-8001-f1116de929de"
        request.nonce = "nonce"
        request.scope = "openid name" //Перечесление scope через пробел
        request.state = "state"
        request.redirectUri = "sbercloud://sberidauth"
        SBKAuthManager.auth(withSberId: request)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
