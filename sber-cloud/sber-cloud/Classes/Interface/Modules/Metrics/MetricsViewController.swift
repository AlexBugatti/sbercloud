//
//  MetricsViewController.swift
//  sber-cloud
//
//  Created by Александр on 27.02.2021.
//

import UIKit

class MetricsViewController: BaseViewController {

    private lazy var scrollView: UIScrollView = {
        var view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    private lazy var stackView: UIStackView = {
        var view = UIStackView()
        view.axis = .vertical
        view.distribution = .equalSpacing
        view.spacing = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
        loadDashboard()
        // Do any additional setup after loading the view.
    }
    
    func loadDashboard() {
        let now = Date()
        let yesterday = Date.init(timeIntervalSince1970: now.timeIntervalSince1970 - 86900)
        
        showActivity()
        MetricService.getFavoriteMetrics(ids: []) { (resposes) in
            self.hideActivity()
            resposes.forEach { (r) in
                let plotView = PlotView.init(metric: r)
                plotView.translatesAutoresizingMaskIntoConstraints = false
                self.stackView.addArrangedSubview(plotView)
//                plotView.layout.heightEqualTo(200)
            }
        }
//        API.CloudEyeModule.get(metric: .cpu, from: now, to: yesterday, completion: { metric in
//            print(metric)
//            self.hideActivity()
//            if let metric = metric {
//                let plotView = PlotView.init(metric: metric)
//                self.stackView.addArrangedSubview(plotView)
//                plotView.layout.heightEqualTo(200)
//
//                let plotView2 = PlotView.init(metric: metric)
//                self.stackView.addArrangedSubview(plotView2)
//                plotView2.layout.heightEqualTo(200)
//
//                let plotView3 = PlotView.init(metric: metric)
//                self.stackView.addArrangedSubview(plotView3)
//                plotView3.layout.heightEqualTo(200)
//
//            }
//
//        })
    }

    private func configure() {
        self.navigationItem.title = "Метрики"
        self.tabBarItem.title = "Метрики"
        
        view.addSubview(scrollView)
        scrollView.addSubview(stackView)
        
        scrollView.layout.fillSuperview()
        stackView.layout.fillSuperview()
        stackView.layout.widthEqualToSuperview()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
