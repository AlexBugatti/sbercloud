//
//  ProfileViewController.swift
//  sber-cloud
//
//  Created by Александр on 28.02.2021.
//

import UIKit

class ProfileViewController: UIViewController {

    var userInfo: [Profile] = ProfileModel.mock()
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableView.dataSource = self
            self.tableView.delegate = self
            self.tableView.register(UINib.init(nibName: "TextCell", bundle: nil), forCellReuseIdentifier: "TextCell")
            self.tableView.contentInset = UIEdgeInsets.init(top: 10, left: 0, bottom: 0, right: 0)
        }
    }
    @IBOutlet weak var logoutButton: UIButton! {
        didSet {
            self.logoutButton.layer.cornerRadius = 10
            self.logoutButton.layer.masksToBounds = true
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "Профиль"
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextCell") as! TextCell
        let profile = userInfo[indexPath.row]
        cell.titleLabel.text = profile.title
        cell.valueLabel.text = profile.value
        
        return cell
    }

}
