//
//  MetricCell.swift
//  sber-cloud
//
//  Created by Александр on 28.02.2021.
//

import UIKit

class MetricCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var activeView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        activeView.layer.cornerRadius = 8
        activeView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
