//
//  TreeTableViewCell.swift
//  sber-cloud
//
//  Created by Александр on 28.02.2021.
//

import UIKit

class TreeTableViewCell: UITableViewCell {

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(icon: UIImage? = nil, title: String) {
        self.iconView.image = icon
        self.titleLabel.text = title
    }
    
}
