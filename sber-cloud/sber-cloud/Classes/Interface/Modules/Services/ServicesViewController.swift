//
//  ServicesViewController.swift
//  sber-cloud
//
//  Created by Александр on 28.02.2021.
//

import UIKit

class CatalogStorage {
    var namespace: String
    var icon: UIImage?
    var order: Int?
    var category: String
    var service: String
    var metric: [ServiceModel]
    
    init(namespace: String, metrics: [ServiceModel]) {
        self.category = MetricService.categories[namespace] ?? "null"
        self.service = MetricService.services[namespace] ?? "Unknown Service"
        self.namespace = namespace
        self.metric = metrics
    }
}

class ServicesViewController: BaseViewController {

    var catalog: [CatalogStorage] = [] {
        didSet {
            self.tableView?.reloadData()
        }
    }
    var services: [ServiceModel] = []
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            self.tableView.delegate = self
            self.tableView.dataSource = self
            self.tableView.register(UINib.init(nibName: "TreeTableViewCell", bundle: nil), forCellReuseIdentifier: "TreeTableViewCell")
            self.tableView.register(UINib.init(nibName: "MetricCell", bundle: nil), forCellReuseIdentifier: "MetricCell")
            self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
        loadCatalog()
        // Do any additional setup after loading the view.
    }
    
    func configure() {
        self.navigationItem.title = "Сервисы"
    }
    
    func groupping(services: [ServiceModel]) -> [CatalogStorage] {
//        var sys: [String: [ServiceModel]] = ["SYS.ECS": [], "AFT.ECS": [], "SYS.AS": [], "SYS.EVS": [], "SYS.OBS": [], "SYS.SFS": [], "SYS.VPC": [], "SYS.ELB": []]
        let set = Set(services.compactMap({ $0.namespace }))
        let namespaces = set.map({ String($0) })
        let catalog = namespaces.compactMap { (namespace) -> CatalogStorage? in
            let nservices = services.filter({ $0.namespace == namespace })
            let catalog = CatalogStorage.init(namespace: namespace, metrics: nservices)
            return catalog
        }
        
        return catalog
    }

    func loadCatalog() {
        showActivity()
        API.CloudEyeModule.getCatalog { (services) in
            self.hideActivity()
            self.services = services
            self.catalog = self.groupping(services: services)
            print(services)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ServicesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.catalog.count
    }
//
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let s = self.catalog[section]
        return s.metric.count
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        let keys = self.catalog.keys.enumerated() as [String]
//        let key = keys[section]
//        return grou
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let service = self.catalog[section]
        let cell = tableView.dequeueReusableCell(withIdentifier: "TreeTableViewCell") as! TreeTableViewCell
        cell.configure(icon: UIImage(named: service.service), title: service.service)
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let service = self.catalog[indexPath.section]
        let metric = service.metric[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "MetricCell") as! MetricCell
        cell.titleLabel.text = metric.metric_name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let alert = UIAlertController.init(title: "Добавить в Дашборд", message: "Вы действительно хотите добавить метрику?", preferredStyle: .alert)
        let simple = UIAlertAction.init(title: "Отмена", style: .default) { (action) in
            //
        }
        let sber = UIAlertAction.init(title: "Да", style: .default) { (action) in
//            self.createSberRequest()
        }
        alert.addAction(simple)
        alert.addAction(sber)
        self.present(alert, animated: true, completion: nil)
    }

}
