//
//  MainRouter.swift
//  sber-cloud
//
//  Created by Александр on 27.02.2021.
//

import Foundation
import UIKit

class MainRouter {
    
    var auth = false
    
    var didOpenDashboard: (()->Void)?
    var rootController: UIViewController
    
    var authRouter = AuthRouter()
    var tabRouter = TabRouter(tabController: UITabBarController())
    
    init() {
        if auth {
            rootController = tabRouter.tabController
        } else {
            rootController = authRouter.navigationController
        }
        configure()
    }
    
    func configure() {
        authRouter.didOpenDashboard = {
            self.auth = true
            self.rootController = self.tabRouter.tabController
            self.didOpenDashboard?()
        }
    }
    
    func gotodashboard() {
        self.rootController = self.tabRouter.tabController
        self.didOpenDashboard?()
    }
    
}

class TabRouter {
    var tabController: UITabBarController
    
    init(tabController: UITabBarController) {
        self.tabController = tabController
        
        let vc = MetricsViewController()
        let metricVC = UINavigationController(rootViewController: vc)
        metricVC.navigationBar.isTranslucent = false
        let servicesVC = UINavigationController(rootViewController: ServicesViewController())
        let profileVC = UINavigationController(rootViewController: ProfileViewController())

        tabController.viewControllers = [metricVC, servicesVC, profileVC]
        
        tabController.tabBar.items?[0].image = R.image.dashboardPassive()?.withRenderingMode(.alwaysOriginal)
        tabController.tabBar.items?[0].selectedImage = R.image.dashboardActive()?.withRenderingMode(.alwaysOriginal)
        
        tabController.tabBar.items?[1].selectedImage = R.image.metricsActive()?.withRenderingMode(.alwaysOriginal)
        tabController.tabBar.items?[1].image = R.image.metricsPassive()?.withRenderingMode(.alwaysOriginal)
        
        tabController.tabBar.items?[2].selectedImage = R.image.userActive()?.withRenderingMode(.alwaysOriginal)
        tabController.tabBar.items?[2].image = R.image.userPassive()?.withRenderingMode(.alwaysOriginal)
        tabController.tabBar.isTranslucent = false
        
        tabController.tabBar.items?[0].title = "Дашборд"
        tabController.tabBar.items?[1].title = "Сервисы"
        tabController.tabBar.items?[2].title = "Профиль"
        
   
    }
}

class AuthRouter {
    var didOpenDashboard: (()->Void)?
    var navigationController: UINavigationController
    
    let authVC = AuthViewController()
    
    init() {
        let navController = UINavigationController(rootViewController: authVC)
        navController.setNavigationBarHidden(true, animated: false)
        self.navigationController = navController
        configure()
    }

    func configure() {
        authVC.didAuthSuccess = {
            self.didOpenDashboard?()
        }
    }
    
}
