//
//  MetricModel.swift
//  sber-cloud
//
//  Created by Александр on 28.02.2021.
//

import UIKit

class MetricUnit: Decodable {
    var min: Double
    var timestamp: Int64
    var unit: String
}

class MetricResponse: Decodable {
    var datapoints: [MetricUnit]
    var metric_name: String
}
