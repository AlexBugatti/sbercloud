//
//  ProfileModel.swift
//  sber-cloud
//
//  Created by Александр on 28.02.2021.
//

import UIKit

struct Profile {
    var value: String
    var title: String
}

class ProfileModel: NSObject {

    static func mock() -> [Profile] {
        
        let name = Profile(value: "hackaton103", title: "Имя пользователя")
        let plan = Profile(value: "365 руб./ч", title: "Тарифный план")
        let card = Profile(value: "Sber **** **** **** 7653", title: "Карта для оплаты")
        let id = Profile(value: "429892a84b34f9034a904", title: "ID Аккаунта")
        
        let account = Profile(value: "hackaton103", title: "Имя Аккаунта")
        let email = Profile(value: "k***i@yandex.ru", title: "Email")
        let password = Profile(value: "********", title: "Пароль")
        
        return [name, plan, card, id, account, email, password]
    }
    
}
