//
//  ServiceModel.swift
//  sber-cloud
//
//  Created by Александр on 28.02.2021.
//

import Foundation

class ServiceModel: Decodable {
    var namespace: String
    var dimensions: [Dimension]
    var unit: String?
    var metric_name: String
}

class Dimension: Decodable {
    var name: String
    var value: String
}
