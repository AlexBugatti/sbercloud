//
//  MetricData.swift
//  sber-cloud
//
//  Created by Александр on 28.02.2021.
//

import UIKit

class MetricService: NSObject {

    static var categories = ["SYS.ECS": "Computing", "AGT.ECS": "Computing", "SYS.AS": "Computing", "SYS.EVS": "Storage", "SYS.OBS": "Storage", "SYS.SFS": "Storage", "SYS.VPC": "Network", "SYS.ELB": "Network", "SYS.NAT": "Network", "SYS.DMC": "Application", "SYS.DCS": "Application", "SYS.RDS": "Database", "SYS.DDS": "Database", "SYS.ES": "Enterprice"]
    static var services = ["SYS.ECS": "Elastic Cloud Service", "AGT.ECS": "ECS OS Monitoring", "SYS.AS": "Auto caling", "SYS.EVS": "Elastic Volume Service", "SYS.OBS": "Object Storage Service", "SYS.SFS": "Scalable File Service", "SYS.VPC": "Elastic IP and bandwidth", "SYS.ELB": "Elastic Load Balance", "SYS.NAT": "NAT Gateway", "SYS.DMC": "Distributed Message Service", "SYS.DCS": "Distributed Cache Service", "SYS.RDS": "Relational Database Service", "SYS.DDS": "Document Database Service", "SYS.ES": "Cloud Search Service"]
    
    class func getFavoriteMetrics(ids: [String], completion: @escaping (([MetricResponse])->Void)) {
        let downloadGroup = DispatchGroup()
        var responses: [MetricResponse] = []
        let metrics: [API.CloudEyeModule.Metric] = [.cpu, .disk_read_bytes_rate, .disk_read_requests_rate, .network_outgoing_bytes_aggregate_rate, .disk_write_requests_rate]
        
        metrics.forEach { (metric) in
            downloadGroup.enter()
            MetricService.loadDashboard(metric: metric) { (response) in
                if let r = response {
                    responses.append(r)
                }
                downloadGroup.leave()
            }
        }
        
        downloadGroup.notify(queue: DispatchQueue.main) {
            completion(responses)
        }
    }
    
    class func loadDashboard(metric: API.CloudEyeModule.Metric, completion: @escaping ((MetricResponse?)->Void)) {
        let now = Date()
        let yesterday = Date.init(timeIntervalSince1970: now.timeIntervalSince1970 - 86900)
        
        API.CloudEyeModule.get(metric: metric, from: now, to: yesterday, completion: { metric in
            print(metric)
            completion(metric)
        })
    }
    
}


