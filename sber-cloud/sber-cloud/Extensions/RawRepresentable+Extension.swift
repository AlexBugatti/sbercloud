//
//  RawRepresentable.swift
//  Beauty Profi
//
//  Created by Артем Рябцев on 22.02.2021.
//  Copyright © 2021 Petr. All rights reserved.
//

import Foundation
protocol NotificationName {
    var name: Notification.Name { get }
}

extension RawRepresentable where RawValue == String, Self: NotificationName {
    var name: Notification.Name {
        get {
            return Notification.Name(self.rawValue)
        }
    }
}

enum Notifications: String, NotificationName {
    case showCartNotification
    case showOrder
}

