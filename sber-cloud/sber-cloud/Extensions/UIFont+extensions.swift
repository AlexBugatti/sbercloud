//
//  UIFont+extensions.swift
//  Beauty Profi
//
//  Created by Petr on 04.07.2020.
//  Copyright © 2020 Petr. All rights reserved.
//

import UIKit

extension UIFont {
    enum SFUIText: String {
        case regular = "SFUIText-Regular"
//        case semibold = "SFUIDisplay-Semibold"
        case medium = "SFUIText-Medium"
        case bold = "SFUIText-Bold"
        case heavy = "SFUIText-Heavy"
    }
    
    class func getSFUITextFont(font: SFUIText = .regular, size: CGFloat = 14) -> UIFont {
        return UIFont(name: font.rawValue, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    class func printFonts() {
        UIFont.familyNames.forEach({
            print("\n--- ", $0, " ---")
            print(UIFont.fontNames(forFamilyName: $0))
        })
    }
}
