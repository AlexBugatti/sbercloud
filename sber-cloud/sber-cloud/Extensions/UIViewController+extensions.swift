//
//  UIViewController+Storyboardable.swift
//  Beauty Profi
//
//  Created by Petr on 16.06.2020.
//  Copyright © 2020 Petr. All rights reserved.
//

import UIKit
import SwiftEntryKit

enum AlertType {
    case ok
    case warning
    case error
    
    var color: UIColor {
        switch self {
        case .ok: return #colorLiteral(red: 0.1803921569, green: 0.8, blue: 0.4431372549, alpha: 1)
        case .warning: return #colorLiteral(red: 0.9529411765, green: 0.6117647059, blue: 0.07058823529, alpha: 1)
        case .error: return #colorLiteral(red: 0.7529411765, green: 0.2235294118, blue: 0.168627451, alpha: 1)
        }
    }
}

protocol Storyboardable: class {
}

extension Storyboardable where Self: UIViewController {
    
    static func storyboardViewController() -> Self {
        let storyboard = UIStoryboard(name: String(describing: Self.self), bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: String(describing: Self.self)) as? Self else {
            fatalError("Could not instantiate initial storyboard with name: \(Self.self)")
        }
        
        return vc
    }
    
    static func xibViewController(name: String? = nil) -> Self {
        return Self(nibName: name, bundle: nil)
    }
}

extension UIViewController: Storyboardable { }

extension UIViewController {
    
    static let keyboardWillShow = NotificationDescriptor(name: UIResponder.keyboardWillShowNotification, convert: KeyboardPayload.init)
    static let keyboardWillHide = NotificationDescriptor(name: UIResponder.keyboardWillHideNotification, convert: KeyboardPayload.init)
    
    static var topViewController: UIViewController? {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        guard var topController = keyWindow?.rootViewController else { return nil }
            
        while let presentedViewController = topController.presentedViewController {
            topController = presentedViewController
        }

        return topController
    }
    
    func showWarningAlert(text: String, title: String = "Ошибка", type: AlertType, action: (() -> ())? = nil) {
        
        var attributes = EKAttributes.topNote
        
        attributes.entryBackground = .color(color: EKColor(type.color))
        //        attributes.popBehavior = .animated(animation: .init(translate: .init(duration: 0.3), scale: .init(from: 1, to: 0.7, duration: 0.7)))
        attributes.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 10, offset: .zero))
        attributes.statusBar = .dark
        attributes.scroll = .enabled(swipeable: true, pullbackAnimation: .jolt)
        //        attributes.positionConstraints.maxSize = .init(width: .constant(value: UIScreen.main.bounds.minY), height: .intrinsic)
        
        let title = EKProperty.LabelContent(text: title, style: .init(font: UIFont.getSFUITextFont(font: .medium, size: 14), color: .white))
        let description = EKProperty.LabelContent(text: text, style: .init(font: UIFont.getSFUITextFont(font: .regular, size: 12), color: .white))
        //        let image = EKProperty.ImageContent(image: #imageLiteral(resourceName: "warningIcon"), size: CGSize(width: 35, height: 35))
        let simpleMessage = EKSimpleMessage(image: nil, title: title, description: description)
        let notificationMessage = EKNotificationMessage(simpleMessage: simpleMessage)
        
        let contentView = EKNotificationMessageView(with: notificationMessage)
        SwiftEntryKit.display(entry: contentView, using: attributes)
        
        action?()
    }
}
