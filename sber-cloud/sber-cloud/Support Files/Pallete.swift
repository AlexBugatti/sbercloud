//
//  Pallete.swift
//  sber-cloud
//
//  Created by Александр on 28.02.2021.
//

import UIKit

struct Pallete {
    static let grey = UIColor.init(hexString: "#D2D2D2")
    static let darkGrey = UIColor.init(hexString: "#343E47")
    static let green = UIColor.init(hexString: "#07E897")
}
